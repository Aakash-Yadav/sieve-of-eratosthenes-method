from setuptools import setup
from Cython.Build import cythonize

setup(
    ext_modules = cythonize("app.pyx")
)


from os import system,listdir 

for i in listdir('.'):
    if i=='build':
        system('rm -rf build/')
    if i=='app.c':
        system(f"rm -rf {i}")
