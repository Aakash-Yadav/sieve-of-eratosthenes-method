from cpython cimport array 
from libc.math cimport sqrt

cdef sieve_generator(int number):
    cdef  short int  x,y;

    cdef array.array result = array.array(
 'i',[x for x in range(number+1)])
    cdef array.array return_result = array.array('i',[])

    result[0]=0;
    result[1]=0;

    for x in range(2,int(sqrt(number))+1):
        if result[x]:
            for y in range(x*x,number,x):
                result[y]=0;

    return_result = array.array("i",
            [x for x in range(number) if result[x]])

    return return_result;

cdef sieve_checking(int number):
    cdef int i,out

    for i in range(2,int(sqrt(number))+1):
        if number%i==0:
           out =0
           break 
        else:
            out = 1
    return out

cpdef check(int x):
    return sieve_checking(x);
cpdef generator(int x):
    return sieve_generator(x);


