
# Cython: Sieve of Eratosthenes method, for computing prime number :
In Mathematics, there are various methods to find the prime and composite numbers such as factorisation and division methods. But in the case of the Sieve of Eratosthenes method, it is easy to list down the prime numbers among a set of numbers in a quick way. 
### Sieve of Eratosthenes :
Sieve of Eratosthenes is a method to find the prime numbers and composite numbers among the group of numbers. This method was introduced by Greek Mathematician Eratosthenes, in the third century B.C.
![alt text](https://www.w3resource.com/w3r_images/Sieve_of_Eratosthenes_animation.gif)

## Flowchart:
![alt text](https://www.w3resource.com/w3r_images/python-data-type-list-exercise-34.png)

## Note :
In mathematics, the sieve of Eratosthenes (Ancient Greek: κόσκινον Ἐρατοσθένους, kóskinon Eratosthénous), one of a number of prime number sieves, is a simple, ancient algorithm for finding all prime numbers up to any given limit.
